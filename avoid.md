# inclusive-terminology/avoid-*

These rules suggest avoiding non-inclusive terms and to instead use terms that are considered more inclusive to better align with industry trends.
