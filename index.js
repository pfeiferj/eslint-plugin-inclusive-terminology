const pluralize = require('pluralize');
const fs = require('fs');
const path = require('path');

const rulesConfig = require('./rules.json');

const customConfigPath = path.join(process.cwd(),'.inclusive-terminology-rules.json');

const customRules = fs.existsSync(customConfigPath) && JSON.parse(fs.readFileSync(customConfigPath));



const pluralOrSingular = term => `(${term}|${pluralize(term)})`;
const firstLetterCapitalized = term => `${term.slice(0,1).toUpperCase()}${term.slice(1)}`

const defaultRegex = term => new RegExp(`([^A-Z]${pluralOrSingular(firstLetterCapitalized(term))}$|[^a-z]${pluralOrSingular(term)}$|[^A-Z]${pluralOrSingular(term.toUpperCase())}$|^${pluralOrSingular(firstLetterCapitalized(term))}$|^${pluralOrSingular(firstLetterCapitalized(term))}[^a-z]|[a-z]${pluralOrSingular(firstLetterCapitalized(term))}|[^a-zA-Z]${pluralOrSingular(term.toUpperCase())}[^a-z]|[^a-zA-Z]${pluralOrSingular(term)}[^a-z]|^${pluralOrSingular(term)}$|^${pluralOrSingular(term.toUpperCase())}$|${pluralOrSingular(term)}[^a-z]|${pluralOrSingular(term.toUpperCase())}[^A-Z])`, 'g');

function setRulesAsErrors(rules) {
  return Object.keys(rules).reduce((acc,ruleName) => {
    acc['inclusive-terminology/'+ruleName] = 2;
    return acc;
  },{});
}

function checkForAvoidedTerm(avoidedTerm, value, options) {
  const { customRegex } = options;
  const regex = customRegex
    ? new RegExp(customRegex, 'g')
    : defaultRegex(avoidedTerm);
  return String(value).match(regex);
}

function reportIssue(context, node, avoidedTerm, suggestedTerm) {
  context.report({
    node,
    message: `Avoid using term ${avoidedTerm}, instead try ${suggestedTerm}`
  });
}

function createEslintRules(){
  return (customRules || rulesConfig).reduce((allRules, rule) => {
    const compiledRule = {
      meta: {
        type: 'suggestion',
        docs: {
          description: `Avoid the use of term ${rule.avoidedTerm}`,
          category: 'stylistic',
          recommended: !!rule.recommended,
          url: rule.url,
          suggestion: rule.suggestion
        }
      },
      create: (context) => ({
        Identifier: (node) => {
          const containsAvoidedTerm = checkForAvoidedTerm(rule.avoidedTerm, node.name, { customRegex: rule.customRegex });
          if(containsAvoidedTerm) {
            reportIssue(context, node, rule.avoidedTerm, rule.suggestion);
          }
        },
        Literal: (node) => {
          const containsAvoidedTerm = checkForAvoidedTerm(rule.avoidedTerm, node.value, { customRegex: rule.customRegex });
          if(containsAvoidedTerm) {
            reportIssue(context, node, rule.avoidedTerm, rule.suggestion);
          }
        },
        Program: (node) => {
          node.comments.forEach((comment) => {
            const containsAvoidedTerm = checkForAvoidedTerm(rule.avoidedTerm, comment.value, { customRegex: rule.customRegex });
            if(containsAvoidedTerm) {
              reportIssue(context, node, rule.avoidedTerm, rule.suggestion);
            }
          });
        }
      })
    };
    allRules['avoid-'+rule.avoidedTerm] = compiledRule;
    return allRules;
  }, {});
}

const allRules = createEslintRules();

module.exports = {
  rules: allRules,
  configs: {
    all: {
      rules: setRulesAsErrors(allRules)
    }
  }
}
